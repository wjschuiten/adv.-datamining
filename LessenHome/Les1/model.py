# Note: implement the below classes...

#IMPORTS

from math import tanh, exp

#functions
#activation
def identity_act_func(a):
    return a

def signum_act_func(a):
    if a <0:
        return -1.0
    elif a > 0:
        return 1.0
    else:
        return 0

def tanh_act_func(a):
    return tanh(a)

#derative

def derivative(func, x, *args, **kwargs):
    delta_x = 1e-4
    func_plus = func(x+delta_x, *args, **kwargs)
    func_min  = func(x-delta_x, *args, **kwargs)
    deriv = (func_plus-func_min)/(2.0*delta_x)
    return deriv


#loss

def quadratic_loss_func(y_hat, y):
    return 0.5*(y-y_hat)**2

def absolute_loss_func(y_hat, y):
     return abs(y-y_hat)

# CLASSES

class Perceptron():

    def __init__(self, dim=2):
        # "dim" equals the dimensionality of the attributes
        self.bias = 0.0
        self.weights = [0.0 for x in range(dim)]
        pass
        
    def __str__(self):
        # Returns an informative description
        result = 'Perceptron():'
        result += '\n  - bias = ' + str(self.bias)
        result += '\n  - weights = ' + str(self.weights)
        return result

    def predict(self, x):
        # "x" contains a list with the attributes of a single instance
        #𝑦̂ = sgn(𝑏 +∑𝑖𝑤𝑖⋅𝑥𝑖)
        pre_act = self.bias + sum(wi * xi for wi, xi in zip(self.weights, x))
        post_act = signum_act_func(pre_act)
        return post_act
        
    def train(self, x, y, alpha=0):
        # "x" contains a list with the attributes of a single instance
        # "y" contains the corresponding correct outcome
        # "alpha" is the learning rate; choose a suitable default value

        # Algemene update regel toepassen: wi + alpha*delta_L/delta_y_hat * delta_y_hat / delta_wi * xi

        # Inzoomen delta_L / delta_y_hat, helling berekenen met 2 datapunten
        # 1ste datapunt op basis van de predict
        pre_activation = self.bias + sum([wi * xi for wi, xi in zip(self.weights, x)])
        post_activation = signum_act_func(pre_activation)
        loss1 = quadratic_loss_func(post_activation,y)

        # 2e punt voor helling pakken, klein getal toevoegen
        post_activation2 = post_activation + 1e-6
        loss2 = quadratic_loss_func(post_activation2, y)

        # Berekening voor helling, delta y_hat = loss2 - loss1 (hoeveel ga ik omhoog), delta L = het toegevoegde getal (hoeveel ga ik opzij)
        gradient_loss = (loss2-loss1) / 1e-6

        #Volgende deel formule, Inzoomen delta_y_hat / delta_wi, helling van de activatie functie, zelfde concept, 2 datapunten pakken op deze grafiek en helling berekenen
        y_hat1 = signum_act_func(pre_activation)
        pre_activation2 = pre_activation + 1e-6
        y_hat2 = signum_act_func(pre_activation2)
        gradient_act = (y_hat2 - y_hat1)/ 1e-6

        self.bias = self.bias +alpha*(y - y_hat1)
        self.weights = [wi + alpha*(y-y_hat1)*xi for wi,xi in zip(self.weights, x)]
    
    def fit(self, xs, ys, alpha=1e-6, epochs=5):
        # "x" contains a nested list with the attributes of multiple instances
        # "y" contains a list with the corresponding correct outcomes
        # "alpha" is the learning rate; choose a suitable default value
        # "epochs" equals the number of epochs
        epoch = 0
        while epoch < epochs or epochs == 0:
            error = 0
            for i, x in enumerate(xs):
                self.train(x, ys[i], alpha)
                error = quadratic_loss_func(self.predict(x), ys[i])
            epoch = epoch + 1
            if error == 0:
                return None
            
            
class LinearRegression():

    def __init__(self, dim=2):
        # "dim" equals the dimensionality of the attributes
        self.bias = 0.0
        self.weights = [0.0 for x in range(dim)]
        pass

    def __str__(self):
        # Returns an informative description
        result = 'Perceptron():'
        result += '\n  - bias = ' + str(self.bias)
        result += '\n  - weights = ' + str(self.weights)
        return result

    def predict(self, x):
        # "x" contains a list with the attributes of a single instance
        # 𝑦̂ = 𝑏 +∑𝑖𝑤𝑖⋅𝑥𝑖
        pre_act = self.bias + sum(wi * xi for wi, xi in zip(self.weights, x))
        post_act = identity_act_func(pre_act)
        return post_act

    def train(self, x, y, alpha=0):
        # "x" contains a list with the attributes of a single instance
        # "y" contains the corresponding correct outcome
        # "alpha" is the learning rate; choose a suitable default value

        # Algemene update regel toepassen: wi + alpha*delta_L/delta_y_hat * delta_y_hat / delta_wi * xi

        # Inzoomen delta_L / delta_y_hat, helling berekenen met 2 datapunten
        # 1ste datapunt op basis van de predict
        pre_activation = self.bias + sum([wi * xi for wi, xi in zip(self.weights, x)])
        post_activation = identity_act_func(pre_activation)
        loss1 = quadratic_loss_func(post_activation, y)

        # 2e punt voor helling pakken, klein getal toevoegen
        post_activation2 = post_activation + 1e-6
        loss2 = quadratic_loss_func(post_activation2, y)

        # Berekening voor helling, delta y_hat = loss2 - loss1 (hoeveel ga ik omhoog), delta L = het toegevoegde getal (hoeveel ga ik opzij)
        gradient_loss = (loss2 - loss1) / 1e-6

        # Volgende deel formule, Inzoomen delta_y_hat / delta_wi, helling van de activatie functie, zelfde concept, 2 datapunten pakken op deze grafiek en helling berekenen
        y_hat1 = identity_act_func(pre_activation)
        pre_activation2 = pre_activation + 1e-6
        y_hat2 = identity_act_func(pre_activation2)
        gradient_act = (y_hat2 - y_hat1) / 1e-6

        self.bias = self.bias + alpha * (y - y_hat1)
        self.weights = [wi + alpha * (y - y_hat1) * xi for wi, xi in zip(self.weights, x)]

    def fit(self, xs, ys, alpha=1e-6, epochs=100):
        # "x" contains a nested list with the attributes of multiple instances
        # "y" contains a list with the corresponding correct outcomes
        # "alpha" is the learning rate; choose a suitable default value
        # "epochs" equals the number of epochs
        epoch = 0
        while epoch < epochs or epochs == 0:
            error = 0
            for i, x in enumerate(xs):
                self.train(x, ys[i], alpha)
                error = quadratic_loss_func(self.predict(x), ys[i])
            epoch = epoch + 1
            if error == 0:
                return None
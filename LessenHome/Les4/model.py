# Note: implement the below classes...

#IMPORTS
import math
from math import tanh, exp
from copy import deepcopy
import random

#functions
#activation
def identity_act_func(a):
    return a

def signum_act_func(a):
    if a <0:
        return -1.0
    elif a > 0:
        return 1.0
    else:
        return 0

def tanh_act_func(a):
    return tanh(a)

def softsign_act_func(a):
    return a / (abs(a) + 1)

def logistic_act_func(a):
    return 1 / (1+exp(-a))

def softplus_act_func(a):
    return math.log(1+exp(a))

def relu_act_func(a):
    if (a >= 0):
        return a
    else:
        return 0

#derative

def derivative(func, delta_x = 1e-4):
    def gradient(x, *args):
            func_plus = func(x + delta_x, *args)
            func_min = func(x - delta_x, *args)
            grad = (func_plus - func_min) / (2.0 * delta_x)
            return grad
    return gradient


#loss

def quadratic_loss_func(y_hat, y):
    return 0.5*(y-y_hat)**2

def absolute_loss_func(y_hat, y):
     return abs(y-y_hat)

def perceptron_loss_func(y_hat, y):
    return max(-y_hat * y, 0)

def crossentropy_loss_func(y_hat, y):
    return -y * pseudolog(y_hat) - (1-y)*pseudolog(1-y_hat)

def pseudolog(x, epsilon = 1e-5):
    if x >= epsilon:
        return math.log(x)
    else:
        return math.log(epsilon) + (x-epsilon)/epsilon

# CLASSES

class Perceptron():

    def __init__(self, dim=2):
        # "dim" equals the dimensionality of the attributes
        self.bias = 0.0
        self.weights = [0.0 for x in range(dim)]
        pass
        
    def __str__(self):
        # Returns an informative description
        result = 'Perceptron():'
        result += '\n  - bias = ' + str(self.bias)
        result += '\n  - weights = ' + str(self.weights)
        return result

    def predict(self, x):
        # "x" contains a list with the attributes of a single instance
        #𝑦̂ = sgn(𝑏 +∑𝑖𝑤𝑖⋅𝑥𝑖)
        pre_act = self.bias + sum(wi * xi for wi, xi in zip(self.weights, x))
        post_act = signum_act_func(pre_act)
        return post_act
        
    def train(self, x, y, alpha=0):
        # "x" contains a list with the attributes of a single instance
        # "y" contains the corresponding correct outcome
        # "alpha" is the learning rate; choose a suitable default value

        # Algemene update regel toepassen: wi + alpha*delta_L/delta_y_hat * delta_y_hat / delta_wi * xi

        # Inzoomen delta_L / delta_y_hat, helling berekenen met 2 datapunten
        # 1ste datapunt op basis van de predict
        pre_activation = self.bias + sum([wi * xi for wi, xi in zip(self.weights, x)])
        post_activation = signum_act_func(pre_activation)
        loss1 = quadratic_loss_func(post_activation,y)

        # 2e punt voor helling pakken, klein getal toevoegen
        post_activation2 = post_activation + 1e-6
        loss2 = quadratic_loss_func(post_activation2, y)

        # Berekening voor helling, delta y_hat = loss2 - loss1 (hoeveel ga ik omhoog), delta L = het toegevoegde getal (hoeveel ga ik opzij)
        gradient_loss = (loss2-loss1) / 1e-6

        #Volgende deel formule, Inzoomen delta_y_hat / delta_wi, helling van de activatie functie, zelfde concept, 2 datapunten pakken op deze grafiek en helling berekenen
        y_hat1 = signum_act_func(pre_activation)
        pre_activation2 = pre_activation + 1e-6
        y_hat2 = signum_act_func(pre_activation2)
        gradient_act = (y_hat2 - y_hat1)/ 1e-6

        self.bias = self.bias +alpha*(y - y_hat1)
        self.weights = [wi + alpha*(y-y_hat1)*xi for wi,xi in zip(self.weights, x)]
    
    def fit(self, xs, ys, alpha=1e-6, epochs=5):
        # "x" contains a nested list with the attributes of multiple instances
        # "y" contains a list with the corresponding correct outcomes
        # "alpha" is the learning rate; choose a suitable default value
        # "epochs" equals the number of epochs
        epoch = 0
        while epoch < epochs or epochs == 0:
            error = 0
            for i, x in enumerate(xs):
                self.train(x, ys[i], alpha)
                error = quadratic_loss_func(self.predict(x), ys[i])
            epoch = epoch + 1
            if error == 0:
                return None
            
            
class LinearRegression():

    def __init__(self, dim=2):
        # "dim" equals the dimensionality of the attributes
        self.bias = 0.0
        self.weights = [0.0 for x in range(dim)]
        pass

    def __str__(self):
        # Returns an informative description
        result = 'Perceptron():'
        result += '\n  - bias = ' + str(self.bias)
        result += '\n  - weights = ' + str(self.weights)
        return result

    def predict(self, x):
        # "x" contains a list with the attributes of a single instance
        # 𝑦̂ = 𝑏 +∑𝑖𝑤𝑖⋅𝑥𝑖
        pre_act = self.bias + sum(wi * xi for wi, xi in zip(self.weights, x))
        post_act = identity_act_func(pre_act)
        return post_act

    def train(self, x, y, alpha=0):
        # "x" contains a list with the attributes of a single instance
        # "y" contains the corresponding correct outcome
        # "alpha" is the learning rate; choose a suitable default value

        # Algemene update regel toepassen: wi + alpha*delta_L/delta_y_hat * delta_y_hat / delta_wi * xi

        # Inzoomen delta_L / delta_y_hat, helling berekenen met 2 datapunten
        # 1ste datapunt op basis van de predict
        pre_activation = self.bias + sum([wi * xi for wi, xi in zip(self.weights, x)])
        post_activation = identity_act_func(pre_activation)
        loss1 = quadratic_loss_func(post_activation, y)

        # 2e punt voor helling pakken, klein getal toevoegen
        post_activation2 = post_activation + 1e-6
        loss2 = quadratic_loss_func(post_activation2, y)

        # Berekening voor helling, delta y_hat = loss2 - loss1 (hoeveel ga ik omhoog), delta L = het toegevoegde getal (hoeveel ga ik opzij)
        gradient_loss = (loss2 - loss1) / 1e-6

        # Volgende deel formule, Inzoomen delta_y_hat / delta_wi, helling van de activatie functie, zelfde concept, 2 datapunten pakken op deze grafiek en helling berekenen
        y_hat1 = identity_act_func(pre_activation)
        pre_activation2 = pre_activation + 1e-6
        y_hat2 = identity_act_func(pre_activation2)
        gradient_act = (y_hat2 - y_hat1) / 1e-6

        self.bias = self.bias + alpha * (y - y_hat1)
        self.weights = [wi + alpha * (y - y_hat1) * xi for wi, xi in zip(self.weights, x)]

    def fit(self, xs, ys, alpha=1e-6, epochs=100):
        # "x" contains a nested list with the attributes of multiple instances
        # "y" contains a list with the corresponding correct outcomes
        # "alpha" is the learning rate; choose a suitable default value
        # "epochs" equals the number of epochs
        epoch = 0
        while epoch < epochs or epochs == 0:
            error = 0
            for i, x in enumerate(xs):
                self.train(x, ys[i], alpha)
                error = quadratic_loss_func(self.predict(x), ys[i])
            epoch = epoch + 1
            if error == 0:
                return None

class Neuron():

    def __init__(self, weights, act_func=identity_act_func, loss_func=quadratic_loss_func):
        # "dim" equals the dimensionality of the attributes
        # "act_func" contains a reference to the activation function
        # "loss_func" contains a reference to the loss function
        self.bias = 0
        self.weights = weights
        self.act_func = act_func
        self.loss_func = loss_func
        pass

    def __str__(self):
        # Returns an informative description
        result = 'Neuron():'
        result += '\n  - bias = ' + str(self.bias)
        result += '\n  - weights = ' + str(self.weights)
        result += '\n  - act_func = ' + str(self.act_func)
        result += '\n  - loss_func = ' + str(self.loss_func)
        return result

    def predict(self, x):
        # "x" contains a list with the attributes of a single instance
        y_hat = self.act_func(self.bias + sum(wi * xi for wi, xi in zip(self.weights, x)))
        return y_hat

    def loss(self, x, y):
        # "x" contains a list with the attributes of a single instance
        # "y" contains the corresponding correct outcome
        pre_activation = self.bias + sum([wi * xi for wi, xi in zip(self.weights, x)])
        post_activation = self.act_func(pre_activation)
        loss = quadratic_loss_func(post_activation,y)
        return loss


    def train(self, x, y, alpha=1e-6):
        # "x" contains a list with the attributes of a single instance
        # "y" contains the corresponding correct outcome
        # "alpha" is the learning rate; choose a suitable default value

        pre_activation = self.bias + sum([wi * xi for wi, xi in zip(self.weights, x)])
        self.bias = self.bias +alpha*derivative(self.loss_func)(self.predict(x), y)*derivative(self.act_func)(pre_activation)
        self.weights = [wi + alpha*derivative(self.loss_func)(self.predict(x), y)*derivative(self.act_func)(pre_activation)*xi for wi,xi in zip(self.weights, x)]

    def fit(self, xs, ys, alpha=1e-6, epochs=100):
        # "x" contains a nested list with the attributes of multiple instances
        # "y" contains a list with the corresponding correct outcomes
        # "alpha" is the learning rate; choose a suitable default value
        # "epochs" equals the number of epochs
        epoch = 0
        while epoch < epochs or epochs == 0:
            error = 0
            for i, x in enumerate(xs):
                self.train(x, ys[i], alpha=alpha)
                # error += ys[i] - self.predict(x)
                error = self.loss(x, ys[i])
            epoch += 1
            if error == 0:
                return


class Layer():

    def __init__(self, next=None):
        # "next" optionally contains a reference to the next neural layer
        self.next = next

    def __str__(self):
        # returns an informative description
        if self.next == None:
            return 'None'
        else:
            return '\\\n' + str(self.next)

    def __add__(self, other):
        # "other" contains a reference to the neural layer to be concatenated
        result = deepcopy(self)
        result.append(deepcopy(other))
        return result

    def append(self, next):
        # "next" contains a reference to the neural layer to be appended
        if self.next == None:
            # if this is the last layer, append the next layer to this layer
            self.next = next
        else:
            # if there is a next layer already, pass the next layer to that
            self.next.append(next)


class FullLayer(Layer):

    def __init__(self, inputs, outputs, act_func=identity_act_func, next=None):
        # "inputs" equals the number of inputs
        # "outputs" equals the number of outputs
        # "act_func" contains a reference to the activation function
        # "next" optionally contains a reference to the next neural layer
        super().__init__(next)
        self.biases = [0.0 for x in range(inputs)]
        self.outputs = outputs
        weight_max = math.sqrt(6/inputs+outputs)
        weight_min = -math.sqrt(6/inputs+outputs)
        self.weights = [[random.uniform(weight_min, weight_max) for x in range(inputs)]for y in range(outputs)]
        self.act_func = act_func
        self.Neurons = [Neuron(self.weights[i], act_func) for i in range(outputs)]

    def __str__(self):
        # returns an informative description
        text = 'FullLayer():'
        text += '\n  - biases = ' + str(self.biases)
        text += '\n  - weights = ' + str(self.weights)
        text += '\n  - act_func = ' + self.act_func.__name__
        text += '\n  - next = ' + super().__str__()
        return text

    def predict(self, x):
        predictions = []
        for n in self.Neurons:
            predictions.append(n.predict(x))
        return self.next.predict(predictions)

    def loss(self, x, y):
        # "x" contains a list with the attributes of a single instance
        # "y" contains a list with the corresponding correct outcomes
        # returns the loss of the prediction
        predictions = []
        for n in self.Neurons:
            predictions.append(n.predict(x))
        y_hat = predictions
        return self.next.loss(y_hat, y)


    def train(self, x, y, alpha=0.0):
        # Calculate the pre-activation values from the input. Just the prediction without activation
        pre_activation = [n.bias + sum([x[i] * n.weights[i] for i in range(len(x))]) for n in self.Neurons]

        # All pre-activation values run throught the activation function
        post_activation = [self.act_func(a) for a in pre_activation]

        # The gradient of the next layer to the loss, which we need to do backpropagation
        post_gradient = self.next.train(post_activation, y, alpha=alpha)

        # The gradient from this layers pre-activation values to the loss
        pre_gradient = [derivative(self.act_func)(pre_activation[i]) * post_gradient[i] for i in
                        range(len(pre_activation))]

        # The gradient from this layers inputs to the loss
        input_gradient = [sum([pre_gradient[i] * self.weights[i][j] for i in range(len(pre_gradient))]) for j in
                          range(len(x))]

        # Updating the bias based on the pre-gradient
        for i, n in enumerate(self.Neurons):
            n.bias -= alpha * pre_gradient[i]


        # Updating the weights based on the pre-gradient
        for i, n in enumerate(self.Neurons):  # Per neuron
            for j in range(len(n.weights)):  # Per weight
                n.weights[j] -= alpha * pre_gradient[i] * x[j]

        return input_gradient

    def fit(self, xs, ys, alpha=1e-6, epochs=100):
        # "x" contains a nested list with the attributes of multiple instances
        # "y" contains a list with the corresponding correct outcomes
        # "alpha" is the learning rate; choose a suitable default value
        # "epochs" equals the number of epochs
        epoch = 0
        while epoch < epochs or epochs == 0:
            error = 0
            for i, x in enumerate(xs):
                self.train(x, ys[i], alpha=alpha)
                # error += ys[i] - self.predict(x)
                error = self.loss(x, ys[i])
            epoch += 1
            if error == 0:
                return


class LossLayer(Layer):

    def __init__(self, loss_func=quadratic_loss_func):
        # "loss_func" contains a reference to the loss-function
        super().__init__()
        self.loss_func = loss_func
        pass

    def __str__(self):
        # returns an informative description
        text = 'LossLayer():'
        text += '\n  - loss_func = ' + self.loss_func.__name__
        return text

    def predict(self, x):
        #x is already fully predicted value of the model
        return x

    def loss(self, x, y):
        # "x" contains a list with the attributes of a single instance
        # "y" contains a list with the corresponding correct outcomes
        # returns the loss of the prediction
        losses = []
        for n in range(len(x)):
            losses.append(self.loss_func(x[n], y[n]))
        return sum(losses)



    def train(self, x, y, alpha=0):
        # "x" contains a list with the attributes of a single instance
        # "y" contains a list with the corresponding correct outcomes
        # "alpha" is the learning rate; choose a suitable default value
        return [derivative(self.loss_func)(x[i], y[i]) for i in range(len(x))]

class SoftmaxLayer(Layer):

    def __init__(self):
        super().__init__()

    def __str__(self):
        # returns an informative description
        text = 'SoftmaxLayer()'
        text += '\n  - next = ' + super().__str__()
        return text

    def predict(self, x):
        # "x" contains a list with the attributes of a single instance
        # returns the predicted outcome
        new_list = []
        for n in x:
            n = n-max(x)
            n = exp(n)
            new_list.append(n)
        y_hat = [i / sum(new_list) for i in new_list]
        return y_hat

    def loss(self, x, y):
        # "x" contains a list with the attributes of a single instance
        # "y" contains a list with the corresponding correct outcomes
        # returns the loss of the prediction
        return self.next.loss(self.predict(x),y)

    def train(self, x, y, alpha=0):
        prediction = self.predict(x)
        output_gradient = self.next.train(prediction, y, alpha)
        input_gradient = [sum([output_gradient[i] * prediction[i] * ((j == i) - prediction[j]) for i in range(len(prediction))]) for j in range(len(prediction))]
        return input_gradient

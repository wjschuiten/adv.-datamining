# Note: implement the below classes...

# IMPORTS:

from math import tanh, log, exp
from copy import deepcopy


# FUNCTIONS:

# +---------------------------+
# | Copy the definitions from |
# | the previous lesson of:   |
# | - activation-functions    |
# | - loss-functions          |
# | - the derivative-function |
# +---------------------------+

# FUNCTIONS:

# Some activation-functions:

def identity_act_func(a):
    return a

def signum_act_func(a):
    if a <0:
        return -1.0
    elif a > 0:
        return 1.0
    else:
        return 0

def tanh_act_func(a):
    return tanh(a)

#def softsign_act_func(a):
#        return softsign(a)

# def logistic_act_func(a):
#     return None

# Some loss-functions:

def quadratic_loss_func(y_hat, y):
    return 0.5*(y-y_hat)**2

def absolute_loss_func(y_hat, y):
     return abs(y-y_hat)

# CLASSES

class Layer():

    def __init__(self, next=None):
        # "next" optionally contains a reference to the next neural layer
        self.next = next

    def __str__(self):
        # Returns an informative description
        if self.next == None:
            return 'None'
        else:
            return '\\\n' + str(self.next)

    def __add__(self, other):
        # "other" contains a reference to the neural layer to be concatenated
        result = deepcopy(self)
        result.append(deepcopy(other))
        return result

    def append(self, next):
        # "next" contains a reference to the neural layer to be appended
        if self.next == None:
            # if this is the last layer, append the next layer to this layer
            self.next = next
        else:
            # if there is a next layer already, pass the next layer to that
            self.next.append(next)


class FullLayer(Layer):

    def __init__(self, inputs, outputs, act_func=identity_act_func, next=None):
        # "inputs" equals the number of inputs
        # "outputs" equals the number of outputs
        # "act_func" contains a reference to the activation function
        # "next" optionally contains a reference to the next neural layer
        super().__init__(next)
        pass

    def __str__(self):
        # Returns an informative description
        result = 'FullLayer():'
        result += '\n  - inputs = ' + str(self.inputs)
        result += '\n  - outputs = ' + str(self.outputs)
        result += '\n  - biases = ' + str(self.biases)
        result += '\n  - weights = ' + str(self.weights)
        result += '\n  - act_func = ' + self.act_func.__name__
        result += '\n  - next = ' + super().__str__()
        return result

    def predict(self, x):
        # "x" contains a list with the attributes of a single instance
        pass
        return None

    def loss(self, x, y):
        # "x" contains a list with the attributes of a single instance
        # "y" contains a list with the corresponding correct outcomes
        pass
        return None


class LossLayer(Layer):

    def __init__(self, inputs, loss_func=quadratic_loss_func):
        # "inputs" equals the number of inputs
        # "loss_func" contains a reference to the loss-function
        super().__init__()
        pass

    def __str__(self):
        # Returns an informative description
        result = 'LossLayer():'
        result += '\n  - inputs = ' + str(self.inputs)
        result += '\n  - loss_func = ' + self.loss_func.__name__
        result += '\n  - next = ' + super().__str__()
        return result

    def predict(self, x):
        # "x" contains a list with the attributes of a single instance
        pass
        return None

    def loss(self, x, y):
        # "x" contains a list with the attributes of a single instance
        # "y" contains a list with the corresponding correct outcomes
        pass
        return None

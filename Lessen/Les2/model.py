# Note: implement the below classes...

# IMPORTS:

from math import tanh, exp


# FUNCTIONS:

# Some activation-functions:

def identity_act_func(a):
    return a

def signum_act_func(a):
    if a <0:
        return -1.0
    elif a > 0:
        return 1.0
    else:
        return 0

def tanh_act_func(a):
    return tanh(a)

# def softsign_act_func(a):
#     return None

# def logistic_act_func(a):
#     return None

# Some loss-functions:

def quadratic_loss_func(y_hat, y):
    return 0.5*(y-y_hat)**2

def absolute_loss_func(y_hat, y):
     return abs(y-y_hat)

# Calculating the numeric derivative:

def derivative(func, x, *args, **kwargs):
    delta_x = 1e-4
    func_plus = func(x+delta_x, *args, **kwargs)
    func_min  = func(x-delta_x, *args, **kwargs)
    deriv = (func_plus-func_min)/(2.0*delta_x)
    return deriv


# CLASSES

class Neuron():

    def __init__(self, dim=2, act_func=identity_act_func, loss_func=quadratic_loss_func):
        # "dim" equals the dimensionality of the attributes
        # "act_func" contains a reference to the activation function
        # "loss_func" contains a reference to the loss function
        pass
        
    def __str__(self):
        # Returns an informative description
        result = 'Neuron():'
        result += '\n  - bias = ' + str(self.bias)
        result += '\n  - weights = ' + str(self.weights)
        result += '\n  - act_func = ' + str(self.act_func)
        result += '\n  - loss_func = ' + str(self.loss_func)
        return result

    def predict(self, x):
        # "x" contains a list with the attributes of a single instance
        pass
        return None
        
    def loss(self, x, y):
        # "x" contains a list with the attributes of a single instance
        # "y" contains the corresponding correct outcome
        pass
        return None
    
    def train(self, x, y, alpha=0):
        # "x" contains a list with the attributes of a single instance
        # "y" contains the corresponding correct outcome
        # "alpha" is the learning rate; choose a suitable default value
        pass
    
    def fit(self, xs, ys, alpha=0, epochs=100):
        # "x" contains a nested list with the attributes of multiple instances
        # "y" contains a list with the corresponding correct outcomes
        # "alpha" is the learning rate; choose a suitable default value
        # "epochs" equals the number of epochs
        pass
        

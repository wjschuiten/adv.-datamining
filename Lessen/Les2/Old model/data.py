#!/usr/bin/python

# Note: no changes need to be made to this file...

# IMPORTS:

from math import sqrt
from random import gauss
import matplotlib.pyplot as plt


# FUNCTIONS:

def generate_data(num=64, dim=2, bias=None, weights=None, noise=0.0, nominal=True):
    """Generate a suitable dataset with attributes and outcomes.

    Keyword arguments:
    num      -- the number of instances (default 64)
    dim      -- the dimensionality of the attributes (default 2)
    bias     -- the bias of the model equation (default random)
    weights  -- the weights of the model equation (default random)
    noise    -- the amount of noise to add (default 0.0)
    nominal  -- generate nominal classes or continuous values (default True)

    Return values:
    xs       -- the num x dim values of the attributes
    ys       -- the num values of the labels
    """
    # Generate random weights if none provided
    if weights == None:
        weights = [gauss(0.0, 1.0) for d in range(dim)]
    # Generate random bias if none provided
    if bias == None:
        bias = gauss(0.0, 5.0*sqrt(sum(w**2 for w in weights)))
    # Generate attribute data
    xs = [[gauss(0.0, 10.0) for d in range(dim)] for n in range(num)]
    # Generate outcomes
    if nominal:
        ys = [-1 if bias+sum(wi*xi for wi, xi in zip(weights, x)) < 0 else 1 for x in xs]
    else:
        ys = [bias+sum(wi*xi for wi, xi in zip(weights, x)) for x in xs]
    # Add noise
    xs = [[xs[n][d]+gauss(0.0, noise) for d in range(dim)] for n in range(num)]
    # Return values
    return xs, ys


def plot_data(xs, ys, model, title='', show=False):
    """Plots data according to true and modeled outcomes.

    Keyword arguments:
    xs       -- the values of the attributes
    ys       -- the values of the true outcomes
    model    -- the Perceptron or LinearRegression model
    show     -- renders all graphics windows on-screen (default false)
    """
    # Open a new figure and determine color range
    fig, ax = plt.subplots()
    v = max(max(ys), -min(ys))
    # Paint background colors denoting the model predictions
    z = [[model.predict([(x/5.0)-30.0, (y/5.0)-30.0]) for x in range(301)] for y in range(301)]
    fig.colorbar(ax.imshow(z, origin='lower', extent=(-30.0, 30.0, -30.0, 30.0), interpolation='bilinear', cmap=plt.cm.RdYlBu, vmin=-v, vmax=v))
    ax.contour([(x/5.0)-30.0 for x in range(301)], [(y/5.0)-30.0 for y in range(301)], z, levels=[0.0], colors='k', linestyles='--', linewidths=1.0)
    # Overlay the actual data points
    x0s = [x[0] for x in xs]
    x1s = [x[1] for x in xs]
    plt.scatter(x=x0s, y=x1s, c=ys, edgecolors='w', cmap=plt.cm.RdYlBu, vmin=-v, vmax=v)
    # Calculate and show the total loss
    total_loss = sum(model.loss(x, y) for x, y in zip(xs, ys))
    plt.text(-29.0, -29.0, 'Loss: {:.3f}'.format(total_loss))
    # Finish and show the figure
    ax.axis([-30.0, 30.0, -30.0, 30.0])
    ax.grid(True, color='k', linestyle=':', linewidth=0.5)
    ax.axhline(y=0, color='k', linestyle='-', linewidth=1.0)
    ax.axvline(x=0, color='k', linestyle='-', linewidth=1.0)
    ax.set_axisbelow(True)
    plt.title(title)
    plt.xlabel(r'$x_1$')
    plt.ylabel(r'$x_2$')
    if show:
        plt.show()


# REDIRECT EXECUTION

if __name__ == "__main__":
    print('Warning: data.py is a module; now running script.py instead...')
    import script
